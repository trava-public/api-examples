# Trava API Examples

## Introduction

This repository contains example scripts for common workflows using the [Trava External API](https://api-docs.travasecurity.com).

A valid Trava API from an active Trava subscription must be used in these scripts

## Running Scripts

### Syncing Azure AD to Phishing Targets with Microsoft Graph

1. Navigate to `node/phishing` folder
2. Run `yarn` to install dependencies
3. Copy `.env.example` to `.env` and fill in variables
4. Copy `data-example.json` to `data.json` and fill in an object with details for each azure graph and associated Trava tenant.
5. Run the script: `node . --op syncUsers --data ./data.json --dryRun`. Dry run will output the changes without making them.
