const msal = require("@azure/msal-node");

const tokenRequest = {
  scopes: [process.env.GRAPH_ENDPOINT + "/.default"],
};

const apiConfig = {
  uri: process.env.GRAPH_ENDPOINT + "/v1.0/users",
};

async function getToken(tenantId, clientId, clientSecret) {
  const msalConfig = {
    auth: {
      clientId,
      authority: process.env.AAD_ENDPOINT + "/" + tenantId,
      clientSecret,
    },
  };

  const cca = new msal.ConfidentialClientApplication(msalConfig);

  return await cca.acquireTokenByClientCredential(tokenRequest);
}

module.exports = {
  apiConfig: apiConfig,
  getToken: getToken,
};
