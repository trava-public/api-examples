// read in env settings
require("dotenv").config();

const yargs = require("yargs");
const syncUsers = require("./sync-users");

const options = yargs
  .usage("Usage: --op <operation_name> --data <data-file-path>")
  .option("op", {
    alias: "operation",
    describe: "operation name",
    type: "string",
    demandOption: true,
  })
  .option("data", {
    alias: "data",
    describe: "data file",
    type: "string",
    demandOption: false,
  })
  .option("dryRun", {
    alias: "dryRun",
    describe: "Dry Run with no changes",
    type: "boolean",
    demandOption: false,
  }).argv;

async function main() {
  console.log(`You have selected: ${options.op}`);

  switch (yargs.argv["op"]) {
    case "syncUsers":
      await syncUsers.syncUsers(yargs.argv);
      break;
    default:
      console.log("Select an operation first");
      break;
  }
}

main();
