const axios = require("axios");

async function callApiGet(endpoint, apiKey) {
  const options = {
    headers: {
      "api-key": `${apiKey}`,
    },
  };

  try {
    const response = await axios.get(endpoint, options);
    return response.data;
  } catch (error) {
    console.log(error);
    return error;
  }
}
async function callApiPost(endpoint, apiKey, body) {
  const options = {
    headers: {
      "api-key": `${apiKey}`,
    },
  };

  try {
    const response = await axios.post(endpoint, body, options);
    return response.data;
  } catch (error) {
    console.log(error);
    return error;
  }
}

async function callApiDelete(endpoint, apiKey, deleteData) {
  const options = {
    headers: {
      "api-key": `${apiKey}`,
    },
    data: deleteData,
  };

  try {
    const response = await axios.delete(endpoint, options);
    return response.data;
  } catch (error) {
    console.log(error);
    return error;
  }
}

async function getPhishingTargets(tenantId) {
  return await callApiGet(
    `${process.env.TRAVA_API_URL}/phishing/targets?tenantId=${tenantId}`,
    process.env.TRAVA_API_KEY
  );
}

async function addPhishingTarget(phishingTarget) {
  return await callApiPost(
    `${process.env.TRAVA_API_URL}/phishing/targets`,
    process.env.TRAVA_API_KEY,
    phishingTarget
  );
}

async function removePhishingTargets(deleteData) {
  return await callApiDelete(
    `${process.env.TRAVA_API_URL}/phishing/targets`,
    process.env.TRAVA_API_KEY,
    { ids: deleteData }
  );
}

module.exports = {
  getPhishingTargets,
  addPhishingTarget,
  removePhishingTargets,
};
