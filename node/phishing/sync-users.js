const graphFetch = require("./graph-api");
const phishing = require("./phishing-api");
const auth = require("./auth");

async function syncUsers(args) {
  try {
    if (args["data"]) {
      const { default: data } = await import(args["data"], {
        assert: {
          type: "json",
        },
      });

      for await (const tenantData of data) {
        console.log(`Syncing ${tenantData.NAME}`);
        // here we get an access token
        const { accessToken } = await auth.getToken(
          tenantData.AZURE_TENANT_ID,
          tenantData.AZURE_CLIENT_ID,
          tenantData.AZURE_CLIENT_SECRET
        );
        // call the web API with the access token
        const graphUsers = await graphFetch
          .callApi(auth.apiConfig.uri, accessToken)
          .then((data) => data.value);

        const phishingUsers = await phishing
          .getPhishingTargets(tenantData.TRAVA_TENANT_ID)
          .then((data) => data.data);

        let toAdd = [];
        let toRemove = [];
        // find the users to add
        for (const user of graphUsers) {
          if (user.userPrincipalName.includes(tenantData.EMAIL_DOMAIN)) {
            if (
              phishingUsers.findIndex(
                (u) => u.email === user.userPrincipalName
              ) == -1
            ) {
              toAdd.push({
                firstName: user.givenName,
                lastName: user.surname,
                email: user.userPrincipalName,
                position: user.jobTitle,
              });
            }
          }
        }

        // find the users to remove
        for (const user of phishingUsers) {
          if (
            graphUsers.findIndex((u) => u.userPrincipalName === user.email) ==
            -1
          ) {
            toRemove.push(user);
          }
        }

        console.log("\t Adding");
        console.log(
          "\t",
          toAdd.map((u) => u.email)
        );
        console.log("\t Removing");
        console.log(
          "\t",
          toRemove.map((u) => u.email)
        );

        if (!args["dryRun"]) {
          console.log(`\t No Dry Run - Updating Data`);
          for await (const addUser of toAdd) {
            await phishing.addPhishingTarget({
              ...addUser,
              tenantId: tenantData.TRAVA_TENANT_ID,
            });
          }

          if (toRemove.length) {
            await phishing.removePhishingTargets(toRemove.map((r) => r.id));
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  syncUsers,
};
